interface IDictionary {
  [index: string]: string;
}

type Element = { tagName: string; className?: string; attributes?: IDictionary };

export function createElement({ tagName, className = "", attributes = {}, }: Element) {
  const htmlElement = document.createElement(tagName);

  if (className) {
    htmlElement.classList.add(className);
  }

  Object.keys(attributes).forEach((name) =>
    htmlElement.setAttribute(name, attributes[name]));

  return htmlElement;
}
