import { callApi } from "../helpers/apiHelper";
import { Fighter, FighterDetails } from "../helpers/mockData";

export async function getFighters() {
  try {
    const endpoint = "fighters.json";
    const apiResult = await callApi(endpoint, "GET");

    return apiResult as Fighter[];
  } catch (error) {
    throw error;
  }
}

export async function getFighterDetails(id: string) {
  try {
    const endpoint = `details/fighter/${id}.json`;
    const apiResult = await callApi(endpoint, "GET");

    return apiResult as FighterDetails;
  } catch (error) {
    throw error;
  }
}
