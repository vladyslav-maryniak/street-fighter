import { createFighter } from "./fighterView";
import { showFighterDetailsModal } from "./modals/fighterDetails";
import { createElement } from "./helpers/domHelper";
import { fight } from "./fight";
import { showWinnerModal } from "./modals/winner";
import { getFighterDetails } from "./services/fightersService";
import { Fighter, FighterDetails } from "./helpers/mockData";

export function createFighters(fighters: Fighter[]) {
  const selectFighterForBattle = createFightersSelector();
  const fighterElements = fighters.map((fighter: Fighter) =>
    createFighter(fighter, showFighterDetails, selectFighterForBattle)
  );

  const fightersContainer = createElement({
    tagName: "div",
    className: "fighters",
  });

  fightersContainer.append(...fighterElements);

  return fightersContainer;
}

const fightersDetailsCache = new Map();

async function showFighterDetails(event: Event, fighter: Fighter) {
  const fullInfo = await getFighterInfo(fighter._id);
  showFighterDetailsModal(fullInfo);
}

export async function getFighterInfo(fighterId: string) {
  // get fighter form fightersDetailsCache or use getFighterDetails function
  return getFighterDetails(fighterId);
}

function createFightersSelector() {
  const selectedFighters = new Map<string, FighterDetails>();

  return async function selectFighterForBattle(
    event: Event,
    fighter: Fighter
  ) {
    const fullInfo = await getFighterInfo(fighter._id);

    let eventTarget = event.target as HTMLInputElement;
    if (eventTarget.checked) {
      selectedFighters.set(fighter._id, fullInfo);
    }
    else {
      selectedFighters.delete(fighter._id);
    }

    if (selectedFighters.size === 2) {
      let fighters = Array.from(selectedFighters.values());
      const winner = fight(fighters[0], fighters[1]);
      showWinnerModal(winner);
    }
  };
}
