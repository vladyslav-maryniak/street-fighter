import { Fighter, FighterDetails } from "./helpers/mockData";

export function fight(firstFighter: FighterDetails, secondFighter: FighterDetails) {
  let attackNumber = 0;
  let players: FighterDetails[] = [
    firstFighter,
    secondFighter
  ]

  while (true) {
    let attacker = players[attackNumber++ % 2];
    let enemy = players[attackNumber % 2];

    enemy.health -= getDamage(attacker, enemy);

    console.log(`Attaker: ${attacker.name} - ${attacker.health}\nEnemy: ${enemy.name} - ${enemy.health}\n`)

    if (enemy.health <= 0) {
      enemy.health = 0;
      return attacker;
    }
  }
}

export function getDamage(attacker: FighterDetails, enemy: FighterDetails) {
  let damage = getHitPower(attacker) - getBlockPower(enemy)
  return damage < 0 ? 0 : damage;
}

export function getHitPower(fighter: FighterDetails) {
  return fighter.attack * ((Math.random() * 2) + 1)
}

export function getBlockPower(fighter: FighterDetails) {
  return fighter.defense * ((Math.random() * 2) + 1)
}
