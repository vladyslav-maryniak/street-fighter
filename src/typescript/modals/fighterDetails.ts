import { createElement } from "../helpers/domHelper";
import { FighterDetails } from "../helpers/mockData";
import { showModal } from "./modal";

export function showFighterDetailsModal(fighter: FighterDetails) {
  const title = "Fighter info";
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

function createFighterDetails(fighter: FighterDetails) {

  const fighterDetails = createElement({
    tagName: "div",
    className: "modal-body",
  });
  const nameElement = createElement({
    tagName: "span",
    className: "fighter-name",
  });
  const attackElement = createElement({
    tagName: "span",
    className: "fighter-attack",
  });
  const defenseElement = createElement({
    tagName: "span",
    className: "fighter-defense",
  });
  const healthElement = createElement({
    tagName: "span",
    className: "fighter-health",
  });
  const imageElement = createElement({
    tagName: "span",
    className: "fighter-image",
  });

  // show fighter name, attack, defense, health, image

  nameElement.innerText = `Name: ${fighter.name}\n`;
  attackElement.innerText = `Attack: ${fighter.attack}\n`;
  defenseElement.innerText = `Defense: ${fighter.defense}\n`;
  healthElement.innerText = `Health: ${fighter.health}\n`;
  imageElement.innerHTML = `<img src="${fighter.source}" alt="fighter image">`;

  fighterDetails.append(nameElement);
  fighterDetails.append(attackElement);
  fighterDetails.append(defenseElement);
  fighterDetails.append(healthElement);
  fighterDetails.append(imageElement);

  return fighterDetails;
}
