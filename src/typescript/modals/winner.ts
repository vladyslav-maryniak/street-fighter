import { createElement } from "../helpers/domHelper";
import { FighterDetails } from "../helpers/mockData";
import { showModal } from "./modal";

export function showWinnerModal(winner: FighterDetails) {
  // show winner name and image
  const title = "Winner info";
  const bodyElement = createWinnerDetails(winner);
  showModal({ title, bodyElement });
}

function createWinnerDetails(winner: FighterDetails) {

  const winnerDetails = createElement({
    tagName: "div",
    className: "modal-body",
  });
  const nameElement = createElement({
    tagName: "span",
    className: "winner-name",
  });
  const imageElement = createElement({
    tagName: "span",
    className: "winner-image",
  });

  nameElement.innerText = `Name: ${winner.name}\n`;
  imageElement.innerHTML = `<img src="${winner.source}" alt="winner image">`;

  winnerDetails.append(nameElement);
  winnerDetails.append(imageElement);

  return winnerDetails;
}
